package com.as201mscareer.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;
import com.as201mscareer.domain.Career;
import com.as201mscareer.testcareer.TestCareer;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
class CareerControllerTest {

    TestCareer testCareer = new TestCareer();
    Career career = new Career();

    public String name = "Análisis de Sistemas";
    public String boss = "Luis Manzo";
    public String area = "Carrera Profesional de Anális de Sistemas";
    public String institute = "I.E.S.T.P Valle Grande";
    public double pension = 350.0;
    public Integer course = 9;
    public Integer semester = 6;
    public String status = "ACTIVO";

    @BeforeAll
    void init() { Logger.getGlobal().log(Level.INFO, "INICIO DE PRUEBAS"); }

    @Test
    @DisplayName("ESTABLER LIMISTES DE LA PENSIÓN")
    void limitPensionCareer() {
        career.setPension(350.0);
        assertTrue(testCareer.limitPensionCareer(career.getPension(), 350.0));
    }

    @Test
    @DisplayName("CALCULAR EL TOTAL A PAGAR POR LA CARRERA")
    void payCareer() {
        career.setPension(350.0);
        career.setSemester(6);
        testCareer.payCareer(career.getPension(), career.getSemester());
    }

    @Test
    @DisplayName("CONSULTAR CARRERAS ACTIVAS")
    void consulActive() {
        career.setName("Análisis de Sistemas");
        career.setStatus("ACTIVO");
        assertTrue(testCareer.consultCareer(career.getStatus()));
    }

    @Test
    @DisplayName("RECONOCER CAMPOS NULOS Y NO NULOS")
    void validateEmptyAndNotEmpty() {
        career.setName("Análisis de Sistemas");
        career.setBoss(null);

        assertNotNull(career.getName());
        Logger.getGlobal().log(Level.INFO, "Nombre de carrera = " + career.getName());

        assertNull(career.getBoss());
        Logger.getGlobal().log(Level.WARNING, "Nombre de jefe = " + career.getBoss());
    }

    @AfterAll
    void end() {
        Logger.getGlobal().log(Level.INFO, "FIN DE PRUEBAS");
    }
}
