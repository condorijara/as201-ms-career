package com.as201mscareer.testcareer;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TestCareer {

    public boolean limitPensionCareer(double pension, double limit) {
        if (pension > limit) {
            Logger.getGlobal().log(Level.WARNING, "Error la Pensión es mayor al limite = " + limit);
            return false;
        }
        Logger.getGlobal().log(Level.INFO, "OK Pensión valida = " + pension);
        return true;
    }

    public double payCareer(double pension, Integer semester) {
        double total = pension * semester;
        Logger.getGlobal().log(Level.INFO, "Total a pagar por la carrera = " + total);
        return total;
    }

    public boolean consultCareer(String status) {
        if (status == "ACTIVO") {
            Logger.getGlobal().log(Level.INFO, "CARRERA ACTIVA");
            return true;
        }
        Logger.getGlobal().log(Level.INFO, "CARRERA INACTIVA");
        return false;
    }
}

