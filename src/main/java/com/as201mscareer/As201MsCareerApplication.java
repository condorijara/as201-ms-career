package com.as201mscareer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class As201MsCareerApplication {

    public static void main(String[] args) {
        SpringApplication.run(As201MsCareerApplication.class, args);
    }

}
