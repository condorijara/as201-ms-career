package com.as201mscareer.repository;

import com.as201mscareer.domain.Career;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CareerRepository extends ReactiveCrudRepository<Career, Long> {
}
