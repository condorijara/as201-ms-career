package com.as201mscareer.controller;

import com.as201mscareer.domain.Career;
import com.as201mscareer.repository.CareerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/career")
public class CareerController {

    @Autowired
    private CareerRepository careerRepository;

    @GetMapping
    public Flux<Career> findCareer() {
        log.info("Mostrando todas las carreras");
        return careerRepository.findAll();
    }

    @GetMapping("/{id}")
    public Mono<Career> findCareerById(@PathVariable Long id) {
        if (id == 0 || id == null) {
            log.error("Error al encontrar la carrera con el ID = " + id);
            return null;
        }
        log.info("Carrera encontrada con el ID = " + id);
        return careerRepository.findById(id);
    }

    @PostMapping("/create")
    public Mono<Career> createCareer(@RequestBody Career career) {
        if (career == null) {
            log.error("Error al crear la carrera = " + career.toString());
            return null;
        } else {
            log.info("Carrera creada = " + career.toString());
        }
        career.setStatus("ACTIVO");
        return careerRepository.save(career);
    }

    @PutMapping("/update")
    public Mono<Career> updateCareer(@RequestBody Career career) {
        if (career.getId() == null) {
            log.error("Error al actualizar la carrera = " + career.toString());
            return null;
        }
        log.info("Carrera actualizada = " + career.toString());
        return careerRepository.findById(career.getId()).map((car) -> {
            car.setId(career.getId());
            car.setName(career.getName());
            car.setBoss(career.getBoss());
            car.setArea(career.getArea());
            car.setInstitute(career.getInstitute());
            car.setPension(career.getPension());
            car.setCourse(career.getCourse());
            car.setSemester(career.getSemester());
            car.setStatus(career.getStatus());
            return car;
        }).flatMap(car -> careerRepository.save(car));
    }

    @GetMapping("/inactive/{id}")
    public Mono<Career> inactiveCareer(@PathVariable Long id) {
        if (id == null) {
            log.error("Error al actualizar la carrera = " + id);
            return null;
        }
        log.info("Carrera eliminada = " + id);
        Career data = new Career();
        return careerRepository.findById(id).map((career) -> {
            data.setId(career.getId());
            data.setName(career.getName());
            data.setBoss(career.getBoss());
            data.setArea(career.getArea());
            data.setInstitute(career.getInstitute());
            data.setPension(career.getPension());
            data.setCourse(career.getCourse());
            data.setSemester(career.getSemester());
            data.setStatus("INACTIVO");
            return data;
        }).flatMap(career -> careerRepository.save(data));
    }
}