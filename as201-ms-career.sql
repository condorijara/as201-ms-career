-- CREAR TABLA career
CREATE TABLE career (
	id SERIAL PRIMARY KEY NOT NULL,
	name VARCHAR(50) NOT NULL,
	boss VARCHAR(100) NOT NULL,
	area VARCHAR(100) NOT NULL,
	institute VARCHAR(100) NOT NULL,
	pension DECIMAL(5,2) NOT NULL,
	course INT NOT NULL,
	semester INT NOT NULL,
	status VARCHAR(15) NOT NULL
);


-- INSERTAR REGISTROS DE UBIGEO
INSERT INTO career
(name,boss,area,institute,pension,course,semester,status)
VALUES
('Análisis de Sistemas','Luis Manzo','Carrera Profesional de Anális de Sistemas','I.E.S.T.P Valle Grande','350.00','9','6','ACTIVO');


SELECT * FROM career;